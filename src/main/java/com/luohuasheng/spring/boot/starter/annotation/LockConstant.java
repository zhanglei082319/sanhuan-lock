package com.luohuasheng.spring.boot.starter.annotation;

public class LockConstant {

    /**
     * 默认锁定键
     */
    public static final String LOCK_KEY = "9779ac37c574551bfb3d8695d76a391d5a824970b7e5d471829e6dcf0be00003";

    /**
     * 锁定key前缀
     */
    public static final String REDIS_LOCK_PREFIX = "REDIS_LOCK_PREFIX:";
}
