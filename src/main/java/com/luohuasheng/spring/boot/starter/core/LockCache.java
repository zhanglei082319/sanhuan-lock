package com.luohuasheng.spring.boot.starter.core;

public interface LockCache {


    boolean lock(String key);


    void unLock(String key);
}
