package com.luohuasheng.spring.boot.starter.config;


import com.luohuasheng.spring.boot.starter.core.DefaultLockCache;
import com.luohuasheng.spring.boot.starter.core.LockAspect;
import com.luohuasheng.spring.boot.starter.core.LockCache;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * 配置
 *
 * @author zl
 */
@Configuration
@Import({LockAspect.class})
public class LuohuashengLockAutoConfig {


    /**
     * 默认锁
     */
    @Bean
    @ConditionalOnMissingBean
    public LockCache lockCache(){
        return new DefaultLockCache();
    }

}
