package com.luohuasheng.spring.boot.starter;

import com.luohuasheng.spring.boot.starter.annotation.Lock;

/**
 *
 */
public class LockTest {

    /**
     * 不作为真实测试，编写代码可供参考
     */
    public void lockTest(){
        edit(new User());
    }

    @Lock(lockKey = {"#user.userId","#user.username"},errorMsg = "当前用户正在修改,请稍等")
    public void edit(User user){
        System.out.println("修改用户成功");
    }

    public static class User{
        private Long userId;

        private String username;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }

}
