package com.luohuasheng.spring.boot.starter.annotation;

import java.lang.annotation.*;

/**
 * 锁
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Lock {


    /**
     * 锁定key 业务键
     */
    String[] lockKey() default LockConstant.LOCK_KEY;


    /**
     * 获取锁失败报错信息
     */
    String errorMsg() default "您手速太快了！";
}
